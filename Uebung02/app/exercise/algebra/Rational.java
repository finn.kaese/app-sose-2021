package exercise.algebra;

public class Rational extends BasicFractions {

  //erstellen der Zähler und nenner Variablen
  long numerator;
  long denominator;

  //richtig setzen der zähler und nenner
  public Rational(long numerator, long denominator) {
    setND(numerator, denominator);
  }

  // den kleinsten gemeinsamen teiler finden
  private long gcd(long N, long D){
    long a = N;
    long b = D;
    long rest;

    // Euklidischeralgoritmus
     while(b != 0){
       rest = a%b;
       a = b;
       b = rest;
     }
    return a;
  }

  //die setND() Methode überschreiben, so das der Zähler und Nenner richtig gesetzt werden und der Nenner nich 0 ist und der Bruch gekürtzt wird.
  @Override
  protected void setND(long numerator, long denominator) {
    if (denominator == 0) {
      throw new IllegalArgumentException("Invalid Denominator");
    } else if (denominator < 0) {
      numerator *= -1;
      denominator *= -1;
    }
    long t = gcd(numerator, denominator);

    this.numerator = numerator/t;
    this.denominator = denominator/t;
  }

  //override der getN() Methode zum setzen des Nenners
  @Override
  public long getN(){
    return this.numerator;
  }

  //override der getD() Methode zum setzen des Zählers
  @Override
  public long getD(){
    return this.denominator;
  }

  //clone um später die eingegeben Brüche zu clonen damit diese nicht verloren gehen.
  @Override
	public Object clone() {
		return new Rational(getN(), getD());
	}

  //Override der toString Methode damit der Bruch aussieht wie ein Bruch (Z/N)
  @Override
  public String toString() {
    //ganze Zahl oder nicht
    if (getD() != 1) {
      return "(" + getN() + "/" + getD() + ")";
    } else {
      return "" + getN();
    }
  }

  // Override der equals Methode um abzufrage ob die Beiden Brüche gleich sind und true/fals ausgeben
  @Override
	public boolean equals(Object obj) {
		setND(getN(), getD());

		Rational rat = (Rational) obj;
		rat.setND(rat.getN(), rat.getD());

		return getN() == rat.getN() && getD() == rat.getD();
	}

  //Override des hashCodes und ausgabe des HashCodes mit BruchS
  @Override
  public int hashCode() {
    setND(getN(), getD());
    return this.toString().hashCode();
  }

}
