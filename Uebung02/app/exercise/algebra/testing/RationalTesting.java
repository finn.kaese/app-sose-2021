package exercise.algebra.testing;

import exercise.algebra.*;

import java.util.*;

//testklasse für Aufgabe 1
public class RationalTesting {

	public static void main(String[] args) {

		// erstellen von zwei Brüchen
		Scanner scan = new Scanner(System.in);


		System.out.println("Zähler des ersten Bruchs");
		int num1 = scan.nextInt();
		System.out.println("Nenner des ersten Bruchs");
		int num2 = scan.nextInt();

		Rational b1 = new Rational(num1, num2);

		System.out.println("Zähler des zweiten Bruchs");
		num1 = scan.nextInt();
		System.out.println("Nenner des zweiten Bruchs");
		num2 = scan.nextInt();
		System.out.println();

		Rational b2 = new Rational(num1, num2);


		System.out.println("Bruch 1 =" + b1 );
		System.out.println( "Bruch 2 =" + b2);

		// Bruch 1 auf c1 clonen & b2 aud c2 clonen damit die Uhrsprungs Brüche nicht verloren gehen
		Rational c1 = (Rational) b1.clone();
		Rational c2 = (Rational) b2.clone();

		// add ausführen
		c1.add(c2);
		// Ergebnis von add ausgeben
		System.out.println("add" + b1 + " + " + b2 + " = " +  c1);

		//zurücksetzen der clone Brüche
		c1 = (Rational) b1.clone();
		c2 = (Rational) b2.clone();

		//subtrahieren der clone Brüche (Bruch 1 - Bruch 2)
		c1.sub(c2);

		// Ergebnis der subtraktion ausgeben
		System.out.println("sub" + b1 + " - " + b2 + " = " +  c1);

		//zurücksetzen der clone Brüche
		c1 = (Rational) b1.clone();
		c2 = (Rational) b2.clone();

		// multipikation der Brüche
		c1.mul(c2);

		// Ausgabe der multiplikation
		System.out.println(b1 + " * " + b2 + " = " +  c1);

		//zurücksetzen der clone Brüche (ich hääte eine methode schreiben sollen)
		c1 = (Rational) b1.clone();
		c2 = (Rational) b2.clone();

		// und aufruf der Divisions Methode
		c1.div(c2);
		// ausgabe der Division
		System.out.println(b1 + " / " + b2 + " = " +  c1);
		System.out.println();


		//nachfrage ob Bruch 1 = Bruch 2 gilt
		System.out.println(b1 + " = " + b2 + ": " + b1.equals(b2));

		// Erstellen des HashCodes für Bruch 1 und dann Bruch 2
		System.out.println("HashCode " + b1 + ": " + b1.hashCode());
		System.out.println("HashCode " + b2 + ": " + b2.hashCode());
		System.out.println();

	}
}
