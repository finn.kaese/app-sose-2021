package exercise.algebra;

public abstract class BasicFractions implements Fractional{

  //set nominator and denominator
  protected abstract void setND(long numerator, long denominator);

  //Funktion zum addieren der zwei Brüche
  public void add(Fractional operand) {
    setND(this.getN()* operand.getD()+ operand.getN()*this.getD(), this.getD()* operand.getD());
  }

  //Funktion zum subtrahieren der zwei Brüche
  public void sub(Fractional operand) {
    Fractional negOp = operand.negation();
    //Bruch1-Bruch2=Bruch1+(-Bruch2)
    add(negOp);
  }
  // miltiplizieren der Brüche
  public void mul(Fractional operand) {
    setND(this.getN()*operand.getN(), this.getD()*operand.getD());
  }
  // dividieren der Brüche
  public void div(Fractional operand){
    mul(operand.reciprocal());
  }
  // negieren des bruches
  public Fractional negation() {
    setND(getN()*-1, getD());
    return this;
  }
  // umkehren des bruches für die division
  public Fractional reciprocal() {
    if (getN() == 0)
    	throw new IllegalArgumentException("Invalid Denominator nach Aufruf reciprocal()");
  // setND methode festlegen
  setND(getD(), getN());
  return this;
  }
}
